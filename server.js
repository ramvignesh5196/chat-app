const mongodb = require('mongodb');
const promise = require('bluebird');
let bodyParser = require('body-parser');
let conf = require('./conf/conf.js');
let routes = require('./source/routes/routes.js');
let sockets = require('./source/sockets/socket.js');
let dbUrl = conf.database.api + "://" + conf.database.host + ":" + conf.database.port + "/" + conf.database.schema;
let app = {};
let expressModule = require('express');
let express = new expressModule();

express.use(bodyParser.json());
express.use(bodyParser.raw());
express.use(bodyParser.urlencoded({extended: true}));
app['conf'] = conf;

/******************MongoDb Connection***************************/

app['mongoDb'] = mongodb;

try {
    app.mongoDb.MongoClient.connect(dbUrl, function (err, db) {
        if (err) throw err;
        console.log("| DB Connected and Created |");
        app['mongoDb']['dbName'] = db.db(conf.database.schema);

    });
} catch (error) {
    console.log("| ", new Date(), " Error in mongoDb Connection |")
}

/******************Server Initialization*********************/

try {
    app['express'] = express;
    var ser = express.listen(conf.api);
    console.log("| Server started @ " + conf.api.host + ":" + conf.api.port + " |");
} catch (error) {
    console.log("| ", new Date(), " Error in Server Connection |")
}
/*********socket initialization************/
var io = require('socket.io')(ser);
const socket = require('socket.io-client')('http://0.0.0.0:8642');
app['io'] = io;
app['socket'] = socket;

/*****************************/
class Server {
    constructor() {
        console.log("| Routes initialization taking place |")
        const routesObj = new routes(app);
        const socketsObj = new sockets(app);
        socketsObj.clientConnection();
        routesObj.init();
    }
}

let serverObject = new Server();
module.exports = Server;