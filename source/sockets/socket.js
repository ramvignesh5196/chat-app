let actionsObj = require('../actions/actions.js');
const servicesObj = require('../services/services.js');

class Sockets {
    constructor(app) {
        this.app = app;
    }

    clientConnection = async () => {
        let serviceInstance = new servicesObj(this.app);
        let sio = this.app.io;

        /**********On new socket connection************/
        this.app.io.on('connection', async function (socket) {
            var communicationDetails = {}
            socket.on('entry', async function (userName) {
                console.log("| Socket Name |", userName);
                communicationDetails = {
                    username: userName,
                    clientId: socket.id
                };

                socket.on('create', function (groupName) {
                    console.log("| Room of the Socket |", groupName);
                    socket.join(groupName)
                    communicationDetails['groupName'] = groupName;
                })

                setTimeout(async function () {

                    console.log("| Input for updating clientId |", communicationDetails);

                    let condition = {
                        userName: communicationDetails.username
                    }, updateValue = {
                        clientId: communicationDetails.clientId,
                        groupName: communicationDetails.groupName ? communicationDetails.groupName : ""

                    }, updateObj = {
                        $set: updateValue
                    }

                    let updateClient = await serviceInstance.update("users", condition, updateObj)

                    if (updateClient && updateClient.modifiedCount != 0) {

                        console.log("| User details are correct and client updated |");
                        condition = {
                            groupName: communicationDetails.groupName
                        }

                        let findGroup = await serviceInstance.findOne("groups", condition)
                        if (findGroup && findGroup.length != 0) {
                            console.log("| Group already exists |");
                        } else {
                            condition['isActive'] = true
                            var addGroup = await serviceInstance.insert("groups", condition)
                        }

                        if ((findGroup && findGroup.length != 0) || (addGroup && addGroup != null)) {
                            console.log("| User and group are added to the DB |");
                            socket.emit('message', 'welcome to socket testing');
                        } else {
                            socket.emit('message', 'Group already exists')
                        }
                    } else {
                        console.log("| User not available |");
                        socket.emit('message', 'invalid user');
                        socket.broadcast.emit('message', 'invalid user connected')
                    }
                }, 7000)
            })

            await socket.on('sendMessage', function (msg) {
                if (msg.clientId) {
                    console.log("| Personal message |", msg)
                    sio.to(msg.clientId).emit('receiveMessage', chatMessage(msg.sender, msg.inputMsg))
                } else {
                    console.log("| Group message |", msg);
                    sio.in(msg.groupName).emit('receiveMessage', chatMessage(msg.sender, msg.inputMsg))
                }
            })

            await socket.on('disconnect', async function () {
                console.log("| Socket details |", socket.id, socket.rooms)
                let condition = {
                    clientId: socket.id
                }, updateValue = {
                    clientId: 1,
                    groupName: 1
                }
                    , updateObj = {
                    $unset: updateValue
                }
                let findGroup = await serviceInstance.findOne("users", condition); // to find the room of the disconnected socket
                if (findGroup && findGroup.length != 0) {
                    let updateUsers = await serviceInstance.update("users", condition, updateObj); // to remove the room and socket details from user

                    condition = {
                        groupName: findGroup.groupName
                    }
                    let findGroupExist = await serviceInstance.findOne("users", condition)
                    if (findGroupExist && findGroupExist.length != 0) {
                        console.log("| some client available in the group |", findGroupExist.groupName);
                        sio.in(findGroup.groupName).emit('receiveMessage', 'An user left the group')
                    } else {
                        console.log("| No user available in the group |", findGroup.groupName);
                        condition = {
                            groupName: findGroup.groupName
                        }, updateValue = {
                            isActive: false
                        }, updateObj = {
                            $set: updateValue
                        }

                        let updateGroup = await serviceInstance.update("groups", condition, updateObj)

                        if (updateGroup && updateGroup.modifiedCount != 0) {
                            console.log("| Group deleted |");
                        } else {
                            console.log("| Group not deleted -problem |")
                        }
                    }
                } else {
                    console.log("| invalid user socket closed |")
                    sio.emit('message', 'invalid user disconnected')
                }
            })

        })

        const chatMessage = (from, text) => {
            return {
                from,
                text,
                time: new Date().getTime()
            };
        };
    }


}

module.exports = Sockets;