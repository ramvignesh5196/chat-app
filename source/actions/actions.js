const servicesObj = require('../services/services.js');
const jwt = require('jsonwebtoken');

class Actions {
    constructor(app) {
        this.app = app;
        this.serviceInstance = new servicesObj(app);
    }

    /************Create a new user***************/
    createUser = async (input) => {

        let responseObj = {};
        if (input && input.body && input.body.userName && input.body.email) {

            console.log("| User details for signUp |", input.body);

            if (input.body.password.length > 8) {

                let findUser = await this.serviceInstance.findOne("users", {userName: input.body.userName})

                if (findUser && findUser.length != 0) {
                    responseObj['status'] = "FAILURE";
                    responseObj['message'] = "User already exists"
                } else {
                    input.body['createdTime'] = new Date().getTime();
                    let createUserResult = await this.serviceInstance.insert("users", input.body);

                    if (createUserResult && createUserResult != null) {
                        responseObj['status'] = "SUCCESS";
                        responseObj['message'] = "User creation is successfull"
                    } else {
                        responseObj['status'] = "FAILURE";
                        responseObj['message'] = "User creation is unsuccessfull"
                    }
                }

            } else {
                responseObj['status'] = "FAILURE";
                responseObj['message'] = "invalid entries"
            }

        } else {
            responseObj['status'] = "FAILURE";
            responseObj['message'] = "Input parameters missing"
        }
        return responseObj
    }

    /************Verify the user***************/
    checkUser = async (input) => {
        let responseObj = {};
        if (input && input.body && input.body.userName && input.body.password) {
            let findCriteria = {
                userName: input.body.userName
            }
            let password = input.body.password;

            let findUser = await this.serviceInstance.findOne("users", findCriteria);

            if (findUser && findUser.length != 0) {
                console.log("| Logged in user details |", findUser)
                responseObj['status'] = findUser.password === password ? "SUCCESS" : "FAILURE";
                responseObj['message'] = findUser.password === password ? "User data available" : "Bad Credentials"

                if (responseObj.status == "SUCCESS") {
                    const jwtToken = jwt.sign(findUser, this.app.conf.jwt_secret, {expiresIn: '1h'})
                    responseObj['accessToken'] = jwtToken
                }
            } else {
                responseObj['status'] = "SUCCESS";
                responseObj['message'] = "User Data not available"
            }
            return responseObj
        } else {
            responseObj['status'] = "FAILURE";
            responseObj['messsage'] = "Input parameters missing";

            return responseObj
        }
    }

    /************Verify access***************/
    authToken = async (req, res, next) => {

        const authHeader = req.headers.authorization;
        const token = authHeader ? authHeader.split(' ')[1] : null;
        if (token == null) {
            return res.sendStatus(401)
        }

        jwt.verify(token, this.app.conf.jwt_secret, (err, user) => {
            if (err) return res.sendStatus(403)
            next()
        })
    }

    /************Sending new personal or group message***************/
    sendNewMsg = async (req, res) => {

        console.log(req.body)
        if (req.body && req.body.userName && (req.body.recipient || req.body.group)) {

            if (!req.body.group) {

                let findCriteria = {
                    userName: req.body.recipient
                }
                let findUser = await this.serviceInstance.findOne("users", findCriteria);

                if (findUser && findUser.length != 0 && findUser.clientId) {
                    console.log("| Receiver details |", findUser);

                    let messageObj = {
                        clientId: findUser.clientId,
                        userName: req.body.recipient,
                        sender: req.body.userName,
                        inputMsg: req.body.message
                    }

                    let sendMessage = this.send(messageObj, "private");
                    return sendMessage
                } else {
                    console.log("| Receiver is not connected |");
                    return (responseObj['message'] = "User not online")
                }
            } else {
                let findCriteria = {
                    groupName: req.body.group ? req.body.group : "",
                    isActive: true
                }

                let findGroup = await this.serviceInstance.findOne("groups", findCriteria)

                if (findGroup && findGroup.length != 0) {
                    console.log("| Group is available |", findGroup.groupName);

                    findCriteria = {
                        userName: req.body.userName
                    }
                    let validUser = await this.serviceInstance.findOne("users", findCriteria)

                    if (validUser && validUser.length != 0 && (validUser.groupName === findGroup.groupName)) {
                        console.log("| User has access to send group message |");

                        let messageObj = {
                            sender: req.body.userName,
                            inputMsg: req.body.message ? req.body.message : "Hi",
                            groupName: req.body.group
                        }

                        let sendMessage = this.send(messageObj, "group");
                        return sendMessage

                    } else {
                        console.log("| User has no access to send message |")
                        return {
                            status: "FAILURE",
                            message: "User has no access"
                        }
                    }

                } else {
                    console.log("| Group not available |");
                    return {
                        status: "FAILURE",
                        message: "Invalid group name"
                    }
                }
            }

        } else {
            console.log("| Missing Parameters |");
            return {
                status: "FAILURE",
                message: "Params missing"
            }
        }

    }

    send = async (messageObj, messageType) => {

        console.log("| Message to send |", messageObj)
        let message = await this.app.socket.emit('sendMessage', messageObj)
        let insertCondition = {
            message: messageObj.inputMsg,
            sender: messageObj.sender,
            messageTime: new Date().getTime()
        }

        messageType == "group" ? insertCondition['messageType'] = 2 : insertCondition['messageType'] = 1;
        messageType == "group" ? insertCondition['groupName'] = messageObj.groupName : insertCondition['userName'] = messageObj.userName;

        let insertMessage = await this.serviceInstance.insert("message_transactions", insertCondition)

        if (insertMessage && insertMessage != null) {
            console.log("| Message logged |");
            return {
                status: "SUCCESS",
                message: "Message sent and logged"
            }
        } else {
            console.log("| Message not logged |");
            return {
                status: "SUCCESS",
                message: "Message sent and not logged"
            }
        }

    }

    /************Getting the messages of an user ***************/
    getMessages = async (req, res) => {
        let responseObj = {};

        if (req && req.query && req.query.userName) {
            let condition = {
                userName: req.query.userName
            }

            let userGroupName = await this.serviceInstance.findOne("users", condition)
            if (userGroupName && userGroupName.groupName) {
                condition = {
                    $or: [{userName: req.query.userName}, {groupName: userGroupName.groupName}]
                }
            }
            let messagesList = await this.serviceInstance.find("message_transactions", condition)
            if (messagesList && messagesList.length != 0) {
                console.log("| User messages available |");
                responseObj['data'] = messagesList;
                responseObj['status'] = "SUCCESS";
                responseObj['message'] = "Messages available"

            } else {
                console.log("| User has no messages yet |", messagesList);
                responseObj['status'] = "SUCCESS";
                responseObj['message'] = "No messages"
            }
        } else {
            console.log("| No required input |");
            responseObj['status'] = "FAILURE";
            responseObj['message'] = "Params missing"
        }
        return responseObj

    }

}

module.exports = Actions;