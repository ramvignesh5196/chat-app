class Services {
    constructor(app) {
        this.app = app;
    }

    insert = async (collectionName, insertCriteria) => {
        console.log("| Entered for inserting a record |", insertCriteria);
        try {
            return await this.app.mongoDb.dbName.collection(collectionName).insertOne(insertCriteria);
        } catch (e) {
            console.log("| Catch in insert method |", e);
            return e
        }
    }

    findOne = async (collectionName, findCriteria) => {
        console.log("| Entered for fetching one |", findCriteria);
        try {
            return await this.app.mongoDb.dbName.collection(collectionName).findOne(findCriteria);
        } catch (e) {
            console.log("| Catch in find method |", e);
            return e
        }
    }

    update = async (collectionName, condition, value) => {
        console.log("| Entered for updating details |", condition, value);
        try {
            return await this.app.mongoDb.dbName.collection(collectionName).updateOne(condition, value);
        } catch (e) {
            console.log('| CATCH in update method |', e)
            return e;
        }
    };

    find = async (collectionName, collectionCriteria) => {
        console.log('| Entered for finding many |', collectionCriteria);
        try {
            return await this.app.mongoDb.dbName.collection(collectionName).find(collectionCriteria).toArray();

        } catch (e) {
            console.log('| CATCH in Find method |', e)
            return e;
        }
    };
}

module.exports = Services;