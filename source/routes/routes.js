let actionsObj = require('../actions/actions.js');

class Routes {
    constructor(app) {
        this.app = app;
        this.actionsInstance = new actionsObj(app);
    }

    init = async () => {
        this.app.express.get('/', async (request, response) => {
            response.send({test: "ok"});
        });

        /***********User Registration***************/
        this.app.express.post('/v1/signIn', async (request, response) => {
            let newUser = await this.actionsInstance.createUser(request);
            response.send(newUser)
        });

        /********User login*********/

        this.app.express.post('/v1/login', async (request, response) => {

            let verifyUser = await this.actionsInstance.checkUser(request);
            response.send(verifyUser)
        });


        /***************User Access - send message********************/
        this.app.express.post('/v1/sendMessage', await this.actionsInstance.authToken, async (request, response) => {
            let sendMsg = await this.actionsInstance.sendNewMsg(request)
            response.send(sendMsg)
        })


        /************User access - Get messages list**************/

        this.app.express.get('/v1/messages', await this.actionsInstance.authToken, async (request, response) => {
            let getMsg = await this.actionsInstance.getMessages(request)
            response.send(getMsg)
        })

        /**********Web Page Initialization***********/

        this.app.express.get('/socket', async (request, response) => {
            response.sendFile(__dirname + '/socket-try.html');
        })


    }
}

module.exports = Routes;