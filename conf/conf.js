module.exports = {
    api: {
        host: '0.0.0.0',
        port: '8642',
        routes: {
            cors: true
        }
    },
    database: {
        api: 'mongodb',
        host: '127.0.0.1',
        port: '12345',
        schema: 'systemTask',
        auth: false,
        username: '',
        password: ''
    },
    jwt_secret: '900a26da66536faccb888a2891d5c6a6d9f4b55da4543632d1e9117a7e02589b8f66e9d8e67d3917dbbb7e1acdab575f0fedaf91023daec656a2ec9c6893efdb',

};